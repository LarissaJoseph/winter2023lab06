import java.util.Random;
public class Die{
	//fields
	private int faceValue;
	private Random random;
	
	//Constructor
	Random rand = new Random();
	public Die(){
		this.faceValue = 1;
		this.random = new Random();
	}
	
	//get method
	public int getFaceValue(){
		return this.faceValue;
	}
	
	public void roll(){
		this.faceValue = rand.nextInt(7)+1;
	}
	
	public String toString(){
		return ""+ this.faceValue;
	}
}
	