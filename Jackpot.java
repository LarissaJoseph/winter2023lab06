public class Jackpot{
	
	public static void main(String[] args){
		System.out.println("Welcome to the Jackpot game!!");
		Board board = new Board();
		boolean gameOver = false;
		int numOfTilesClosed = 0;
		
		while(gameOver == false){
			System.out.println(board);
			board.playATurn();
			if(board.playATurn() == true){
				gameOver = true;
			}
			else{
				numOfTilesClosed += 1;
			}
	}
		if(numOfTilesClosed >= 7){
			System.out.println("Congratulation you reached the jackpot. You WON!!!");
		} 
		else{
			System.out.println("Oh no you lost");
		} 
}
} 