public class Board{
	
	
	//fields
	private Die num1;//(like the autho example???);
	private Die num2;
	private boolean[] tiles;//set of tiles numbered from 1-12 .used to store values that have already been rolled
	
	//Constructor
	Die die = new Die();
	public Board(){
		this.num1 = new Die();
		this.num2 = new Die();
		this.tiles = new boolean[12];//not sure at all 
	}
	
	//question 4  part 2 
	//toString
	public String toString(){
		String tile = "";
		for(int i= 0;i<this.tiles.length;i++)
		{ 
	     if(tiles[i] == false){
			tile+=i;
		 }
		 else{
			 tile+="X";
		 }
		}
		return tile;
	} 
	
	public boolean playATurn(){
		this.num1.roll();
		this.num2.roll();
	
	System.out.println("Here's the first die number: "+this.num1 + " Here's the second die number: "+this.num2); 	
	
	
    int sumOfDice = this.num1.getFaceValue() + this.num2.getFaceValue();
	 
	if(!tiles[sumOfDice-1]){
		tiles[sumOfDice-1] = true;
		System.out.println("Closing tile equal to sum: "+sumOfDice);
		return false;
	}
    if(!tiles[this.num1.getFaceValue()-1]){
		tiles[this.num1.getFaceValue()-1] = true;
		System.out.println("Closing tile with the same value as die one: "+this.num1);
		return false;
	}
	if(!tiles[this.num2.getFaceValue()-1]){
		tiles[this.num2.getFaceValue()-1] = true;
		System.out.println("Closing tile with the same value as die one: "+this.num2);
		return false;
	}
	System.out.println("All the tiles for these values are already shut");
	return true;
	}
}


